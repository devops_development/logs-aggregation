# Logs Aggregation

## Getting started

```bash
helm search repo grafana

helm show chart grafana/loki-stack

helm install loki grafana/loki-stack \
  -n loki-stack \
  --set grafana.enabled=true \
  --create-namespace

k config set-context --current --namespace=loki-stack 

helm uninstall loki

helm install loki grafana/loki-stack \
  -n loki-stack \
  --set grafana.enabled=false 
  
kubectl port-forward svc/loki-grafana 3000:80 -n loki-stack

kubectl get secret -n loki-stack loki-grafana \
    -o jsonpath="{.data.admin-password}" | \
    base64 --decode ; echo

```

##  Some examples LogQL 

```bash
{stream="stderr"} |= ``

{container="query-frontend",namespace="loki-dev"} |= "metrics.go" | logfmt | duration > 1

{namespace="demoproject",stream="stderr"} |= ``

count_over_time({stream="stderr"} |= `level=error` [10m])


```